# include <stdio.h>

//Function to find the maximum profit when increasing the ticket price

void incProfit(int a, int t) {
    
	//When the price increase by 5
	
    int i_P, i_max_P = 0, t_max_P = 0;

    /* i_P = profit in the increasing ticket price condition
       i_max_P = maximum profit after increasing ticket price
       i_t_max_P = the ticket price where he get the maximum profit after increasing
    */
    
    int n = 0;

        while ((a - 20 * n) >= 0)
        {
			//Profit = Income - Expenses
			//Income = attendees * Ticket pricee
			//Expenses = 3 * attendees + 500
			//Profit = (( a * t) * (t + 500) - (3 * (a) + 500 ))
			
            i_P = ((a - 20 * n) * (t + 5 * n) - (3 * (a - 20 * n) + 500));
            if (i_P > i_max_P)
        {
            i_max_P = i_P;
            t_max_P = t + 5 * n;
        }
            else
        {
            //Do nothing
        }
            n++;
        }
        printf("\nWhen increasing ticket price:\n");
        printf("Ticket price = Rs.%d\nMaximum profit = Rs.%d\n\n", t_max_P, i_max_P);
        
}

//Function to find the maximum profit when decreasing the ticket price

void decProfit(int a, int t){

    //When the price decreases by 5
    
    int d_P, d_max_P = 0, d_t_max_P = 0;

    /* d_P = profit in the decreasing ticket price condition
       d_max_P = maximum profit after decreasing ticket price
       d_t_max_P = the ticket price where he get the maximum profit after decreasing
    */

    int n = 0;
        while ((t - 5 * n) >= 0)
        {
            d_P = ((a + 20 * n) * (t - 5 * n) - (3 * (a + 20 * n) + 500));
            if (d_P > d_max_P)
        {
            d_max_P = d_P;
            d_t_max_P = t + 5 * n;
        }
            else
        {
            //Do nothing
        }
            n++;
        }
        printf("\nWhen decreasing ticket price:\n");
        printf("Ticket price = Rs.%d\nMaximum profit = Rs.%d\n\n", d_t_max_P, d_max_P);
}

int main() {

	int a, t;
	
    a = 120; // Attendees
    t = 15; //Ticket Price
	
    incProfit(a, t);
    decProfit(a, t);

return 0;	
}